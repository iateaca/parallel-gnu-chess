import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileWriter;


public class ChessTable {

	/*
	 * Process Chess A data
	 */
	static ProcessBuilder chessA = null;
	static Process chessProcessA = null;
	static String commandA = "./gnuchess";

	static InputStream isChessA = null;
	static InputStreamReader isrChessA = null;
	static BufferedReader brChessA = null;

	static OutputStream osChessA = null;
	static OutputStreamWriter osrChessA = null;
	static BufferedWriter bwChessA = null;

	/*
	 * Process Chess B data
	 */
	static ProcessBuilder chessB = null;
	static Process chessProcessB = null;
	static String commandB = "./gnuchess";

	static InputStream isChessB = null;
	static InputStreamReader isrChessB = null;
	static BufferedReader brChessB = null;

	static OutputStream osChessB = null;
	static OutputStreamWriter osrChessB = null;
	static BufferedWriter bwChessB = null;

	static OutputStream osLog = null;
	static OutputStreamWriter osrLog = null;
	static BufferedWriter bwLog = null;


	static BufferedWriter outLog = null;
	

	public static void initData() {

		try {
			// init data for process chess A
			chessA = new ProcessBuilder(commandA);
			//chessA = new ProcessBuilder("valgrind", "--tool=callgrind", "./gnuchess");
			chessProcessA = chessA.start();

			isChessA = chessProcessA.getInputStream();
			isrChessA = new InputStreamReader(isChessA);
			brChessA = new BufferedReader(isrChessA);

			osChessA = chessProcessA.getOutputStream();
			osrChessA = new OutputStreamWriter(osChessA);
			bwChessA = new BufferedWriter(osrChessA);

			// init data for process chess B
			chessB = new ProcessBuilder(commandB);
			chessProcessB = chessB.start();

			isChessB = chessProcessB.getInputStream();
			isrChessB = new InputStreamReader(isChessB);
			brChessB = new BufferedReader(isrChessB);

			osChessB = chessProcessB.getOutputStream();
			osrChessB = new OutputStreamWriter(osChessB);
			bwChessB = new BufferedWriter(osrChessB);

			osLog = chessProcessB.getOutputStream();
			osrLog = new OutputStreamWriter(osLog);
			bwLog = new BufferedWriter(osrLog);


			FileWriter fstream = new FileWriter("LogChessTable.txt", false);
			outLog = new BufferedWriter(fstream);

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean isFinished(String line) {
		return  line.toLowerCase().contains("Draw".toLowerCase()) ||
			line.toLowerCase().contains("Stalemate".toLowerCase()) ||
			line.toLowerCase().contains("mates".toLowerCase());
	}

	public static void main(String args[]) {

		String line;
		initData();

		try {

			// read garbage informations
			for (int i = 0; i < 5; i ++) {
				line = brChessA.readLine();
				line = brChessB.readLine();
			}

			line = "go" + '\n';

			// send to A go command; A move first
			bwChessA.write(line);
			bwChessA.flush();

			// read new line
			line = brChessA.readLine();

			//read response from A (the first move of A)
			line = brChessA.readLine();
			line += '\n';
			System.out.print("A: " + line);

			while (true) {
				
				// send to B the command received from A
				bwChessB.write(line);
				bwChessB.flush();

				// read two new lines
				line = brChessB.readLine();
				outLog.write("B: " + line + "\n");
				outLog.flush();
				line = brChessB.readLine();
				outLog.write("B: " + line + "\n");
				outLog.flush();
				if (isFinished(line)) {
					System.out.print(line + "\n");
					break;
				}

				//read response from B
				line = brChessB.readLine();
				outLog.write("B: " + line + "\n");
				outLog.flush();
				line += '\n';
				System.out.print("B: " + line);
				
				// send to A the command received from B
				bwChessA.write(line);
				bwChessA.flush();

				// read two new lines
				line = brChessA.readLine();
				outLog.write("A: " + line + "\n");
				outLog.flush();
				line = brChessA.readLine();
				outLog.write("A: " + line + "\n");
				outLog.flush();
				if (isFinished(line)) {
					System.out.print(line + "\n");
					break;
				}

				//read response from A
				line = brChessA.readLine();
				outLog.write("A: " + line + "\n");
				outLog.flush();
				line += '\n';
				System.out.print("A: " + line);

			}

			// TODO clean the garbage
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
